<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Carbon\Carbon;
use App\Models\RouteLimitStat;

class RouteLimit
{
    private $route;

    public function __construct()
    {
        $this->route = substr(\request()->getPathInfo(), 1);
    }

    public function handle($request, \Closure $next, ...$params)
    {
        if (empty(\Auth()->id())) {
            throw new \RuntimeException('Auth::id() is not set in RouteLimit');
        }

        foreach ($params as $param) {
            $param = explode('|', $param);
            $limit = (int)$param[0];
            $time = (int)$param[1];

            if ($this->getTotalCount($time) >= $limit) {
                return response('', 429);
            }
        }

        return $next($request);
    }

    private function getTotalCount($time): int
    {
        return (int)RouteLimitStat::select(\DB::raw('SUM(count) as count'))
            ->where('route', $this->route)
            ->where('user_id', \Auth()->id())
            ->where('datetime', '>', Carbon::now()->subMinute($time))
            ->first()['count'];
    }
}
