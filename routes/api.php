<?php
declare(strict_types=1);

/**
 * @var Dingo\Api\Routing\Router $router
 */
/**
 *
 * Laravel\Horizon\Horizon::auth(function ($request) {
 * dd($request, \Auth::user());
 * });
 */

$router->post('ticket.create', [
    'middleware' => ['route_limit:10|60'],
    'scopes' => 'ticket.create',
    'uses' => 'TicketController@create'
]);