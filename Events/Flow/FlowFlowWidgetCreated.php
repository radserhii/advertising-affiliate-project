<?php
declare(strict_types=1);

namespace App\Events\Flow;

use App\Events\Event;
use App\Models\FlowFlowWidget;
use App\Events\RouteLimitEventTrait;

class FlowFlowWidgetCreated extends Event
{
    use RouteLimitEventTrait;

    /**
     * @var FlowFlowWidget
     */
    public $flow_widget;
    public $route;
    public $user_id;
    public $datetime;

    public function __construct(FlowFlowWidget $flow_widget)
    {
        $this->flow_widget = $flow_widget;
        $this->route = $this->getRoute();
        $this->user_id = $this->getUserID();
        $this->datetime = $this->getDateTime();
    }
}
