<?php
declare(strict_types=1);

namespace App\Events\Flow;

use App\Models\Flow;
use App\Events\Event;
use App\Events\RouteLimitEventTrait;

class FlowCreated extends Event
{
    use RouteLimitEventTrait;

    /**
     * @var Flow
     */
    public $flow;
    public $route;
    public $user_id;
    public $datetime;

    public function __construct(Flow $flow)
    {
        $this->flow = $flow;
        $this->route = $this->getRoute();
        $this->user_id = $this->getUserID();
        $this->datetime = $this->getDateTime();
    }
}
