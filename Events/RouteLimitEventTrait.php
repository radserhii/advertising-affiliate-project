<?php
declare(strict_types=1);

namespace App\Events;

use Carbon\Carbon;

trait RouteLimitEventTrait
{
    private function getRoute()
    {
        return substr(\request()->getPathInfo(), 1);
    }

    private function getUserID()
    {
        return \Auth()->id();
    }

    private function getDateTime()
    {
        return Carbon::now()->format('Y-m-d H:i:00');
    }
}