<?php
declare(strict_types=1);

namespace App\Integrations\Nutralyfe;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use App\Models\{Integration, Lead};
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NutralyfeUpdateOrderList extends Command
{
    private const INTEGRATION_TITLE = 'Nutralyfe';

    protected $signature = 'nutralyfe:update_order_list';
    protected $description = '';

    public function handle()
    {
        $integrations = (new Integration())->getActiveByTitle(self::INTEGRATION_TITLE);

        foreach ($integrations as $integration) {
            Lead::whereIntegration($integration)
                ->new()
                ->whereIntegrated()
                ->chunk(50, function (Collection $leads) use ($integration) {

                    $params = $this->getRequestParams($leads, $integration);

                    $result = sendIntegrationRequest(
                        'https://www.nutralyfe.in/tporders/index.php?f=orderDetails',
                        'POST',
                        $params
                    );

                    $orders = json_decode($result['response'], true);

                    foreach ($orders as $order) {
                        if ($order['status'] === 'hold') {
                            continue;
                        }

                        try {
                            $lead = Lead::find(getIdFromHash($order['order_id']));

                            if ($order['status'] === 'approved') {
                                $lead->approve();

                            } elseif ($order['status'] === 'rejected') {
                                $lead->cancel();
                            }

                        } catch (ModelNotFoundException $e) {
                            continue;
                        }
                    }
                });
        }
    }

    private function getRequestParams(Collection $leads, Integration $integration): array
    {
        return [
            'api_key' => $integration['integration_data_array']['api_key'],
            'login_id' => $integration['integration_data_array']['login_id'],
            'order_ids' => implode(',', $leads->pluck('hash')->toArray()),
        ];
    }
}
