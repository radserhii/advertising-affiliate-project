<?php
declare(strict_types=1);

namespace App\Integrations\Nutralyfe;

use App\Models\Lead;
use Illuminate\Bus\Queueable;
use App\Integrations\ReleaseJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Exceptions\Integration\BadResponse;
use Illuminate\Queue\{SerializesModels, InteractsWithQueue};

class NutralyfeAddOrder implements ShouldQueue
{
    use Queueable;
    use SerializesModels;
    use InteractsWithQueue;
    use ReleaseJob;

    const REQUEST_URL = 'https://www.nutralyfe.in/tporders/createOrderByMobile.php';

    /**
     * @var Lead
     */
    private $lead;
    /**
     * @var int
     */
    private $lead_id;

    public function __construct(int $lead_id)
    {
        $this->lead_id = $lead_id;
    }

    public function handle(): void
    {
        $this->lead = Lead::findOrFail($this->lead_id);

        if ($this->lead->cannotIntegrate()) {
            return;
        }

        $params = $this->getRequestParams();
        $result = sendIntegrationRequest(self::REQUEST_URL, 'POST', $params);
        $response = json_decode($result['response'], true);

        $this->insertLog(self::REQUEST_URL, $params, $response);

        if (!isset($response['status'])) {
            $this->releaseJob();
            return;
        }

        if ($response['status'] !== 'ok') {
            throw new BadResponse($this->lead->integration['title'], $response);
        }

        $this->lead->setAsIntegrated($this->lead->generateExternalKeyById());
    }

    private function getRequestParams(): array
    {
        return [
            'api_key' => $this->lead->integration['integration_data_array']['api_key'],
            'login_id' => $this->lead->integration['integration_data_array']['login_id'],
            'order_id' => $this->lead['hash'],
            'goods_id' => $this->lead->target_geo_rule['integration_data_array']['goods_id'],
            'name' => $this->lead->order->getFullName(),
            'phone' => $this->lead->order->getPhone(),
            'email' => $this->lead->order->getEmail(),
            'city' => $this->lead->order->getCity(),
            'address' => $this->lead->order->getAddress(),
            'zone_id' => $this->lead->order->getState(),
            'postcode' => $this->lead->order->getZipcode(),
        ];
    }

    private function insertLog($url, $params, $response)
    {
        $log = "-----\n"
            . "Method:order.add\n"
            . 'Date:' . date('d.m.Y H:i:s') . "\n"
            . "Url:{$url}\n"
            . 'Params:' . json_encode($params, JSON_UNESCAPED_UNICODE) . "\n"
            . 'Response:' . json_encode($response, JSON_UNESCAPED_UNICODE)
            . "\n";

        \File::append(storage_path('/logs/nutralyfe.log'), $log);
    }
}
