<?php
declare(strict_types=1);

namespace App\Integrations\Illusion;

use App\Models\Lead;
use Illuminate\Bus\Queueable;
use App\Integrations\ReleaseJob;
use Illuminate\Queue\{
    SerializesModels, InteractsWithQueue
};
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Exceptions\Integration\BadResponse;
use App\Exceptions\Integration\IntegrationError;

class IllusionAddOrder implements ShouldQueue
{
    use Queueable;
    use InteractsWithQueue;
    use SerializesModels;
    use ReleaseJob;

    public const API_ENDPOINT = 'http://data.wellnuss.site/api/order/';

    /**
     * @var Lead
     */
    private $lead;
    /**
     * @var int int
     */
    private $lead_id;

    public function __construct(int $lead_id)
    {
        $this->lead_id = $lead_id;
    }

    public function handle(): void
    {
        $this->lead = Lead::findOrFail($this->lead_id);
        if ($this->lead->cannotIntegrate()) {
            return;
        }

        $params = $this->getRequestParams();

        try {
            $response = sendIntegrationRequest(self::API_ENDPOINT . '?' . $params);
        } catch (IntegrationError $e) {
            $this->releaseJob();
            return;
        }

        $response = json_decode($response['response'], true);

        $this->insertLog($params, $response);

        if (!isset($response['status']) || !$response['status']) {
            throw new BadResponse('Illusion', $response);
        }

        $this->lead->setAsIntegrated($this->lead->generateExternalKeyById());
    }

    private function getRequestParams(): string
    {
        return http_build_query([
            'name' => $this->lead->order->getFullName(),
            'phone' => $this->lead->order->getPhone(),
            'email' => $this->lead->order->getEmail(),
            'address' => $this->lead->order->getAddress(),
            'order_id' => $this->lead['hash'],
            'publisher_id' => $this->lead->publisher['pseudo_id'],
            'api_key' => $this->lead->integration['integration_data_array']['api_key'],
            'supplier_id' => $this->lead->integration['integration_data_array']['supplier_id'],
            'product_id' => $this->lead->target_geo_rule['integration_data_array']['product_id']
        ]);
    }

    private function insertLog(string $qs, $response)
    {
        $log = "-----\n"
            . 'Date: ' . date('d.m.Y H:i:s') . "\n"
            . 'Request: ' . self::API_ENDPOINT . '?' . $qs . "\n"
            . 'Response: ' . json_encode($response) . "\n"
            . "\n";

        \File::append(storage_path('/logs/illusion.log'), $log);
    }
}
