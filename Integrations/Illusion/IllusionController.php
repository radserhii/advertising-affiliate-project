<?php
declare(strict_types=1);

namespace App\Integrations\Illusion;

use App\Models\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Traits\PostbackinController;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class IllusionController extends Controller
{
    use PostbackinController;

    public function postback(Request $request)
    {
        try {
            $this->validate($request, [
                'api_key' => 'required',
                'status' => 'required|in:' . implode(',', [
                        Lead::APPROVED,
                        Lead::CANCELLED,
                        Lead::TRASHED,
                    ]),
                'lead_hash' => 'required',
            ]);
        } catch (ValidationException $e) {
            return $this->returnValidationError($e->validator);
        }

        try {
            $integration = $this->getIntegration($request);
            $this->appendIntegrationToRequest($integration);

        } catch (ModelNotFoundException $e) {
            return $this->badApiKey();
        }

        try {
            $lead = self::getLeadByHash($integration, $request->input('lead_hash'));
            $this->appendLeadToRequest($lead);

        } catch (ModelNotFoundException $e) {
            return $this->badLead('lead_hash');
        }

        $result = $this->changeLeadStatus($lead, $request->get('status'));

        if ($result instanceof Response) {
            return $result;
        }

        return $this->ok();
    }
}
