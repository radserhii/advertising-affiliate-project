<?php
declare(strict_types=1);

namespace App\Integrations\Illusion;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use App\Exceptions\Lead\IncorrectStatusException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\{Integration, Lead, Traits\PostbackinController};

class IllusionUpdateOrderList extends Command
{
    use PostbackinController;

    private const INTEGRATION_TITLE = 'Illusion';

    protected $signature = 'illusion:update_order_list';
    protected $description = '';

    public function handle()
    {
        $integrations = (new Integration())->getActiveByTitle(self::INTEGRATION_TITLE);

        foreach ($integrations as $integration) {
            Lead::whereIntegration($integration)
                ->new()
                ->whereIntegrated()
                ->chunk(50, function (Collection $leads) {

                    $params = $this->getRequestParams($leads);

                    $result = sendIntegrationRequest('http://wellnuss.site/status.php?' . $params);

                    $orders = json_decode($result['response'], true);

                    foreach ($orders as $order) {
                        if ($order['status'] === 'hold') {
                            continue;
                        }

                        try {
                            $lead = Lead::find(getIdFromHash($order['order_id']));

                            $this->changeLeadStatus($lead, $this->getNewStatus($order['status']));

                        } catch (ModelNotFoundException $e) {
                            continue;
                        }
                    }
                });
        }
    }

    private function getNewStatus(string $status): string
    {
        switch ($status) {
            case 'confirmed':
                return Lead::APPROVED;

            case 'cancel':
                return Lead::CANCELLED;

            case 'trash':
                return Lead::TRASHED;

            default:
                throw new IncorrectStatusException($status);
        }
    }

    private function getRequestParams(Collection $leads): string
    {
        return http_build_query([
            'ids' => implode(',', $leads->pluck('hash')->toArray()),
        ]);
    }
}

