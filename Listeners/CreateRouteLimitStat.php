<?php
declare(strict_types=1);

namespace App\Listeners;

use App\Events\Event;

class CreateRouteLimitStat
{
    public function handle(Event $event)
    {
        \DB::insert(
            'INSERT INTO `route_limit_stats`
				SET `route`    = :route,
					`user_id`  = :user_id,
					`datetime` = :datetime,
					`count`    = 1
				ON DUPLICATE KEY UPDATE
				    `count`     = (`count` + 1);',
            [
                'route' => $event->route,
                'user_id' => $event->user_id,
                'datetime' => $event->datetime,
            ]
        );
    }
}
