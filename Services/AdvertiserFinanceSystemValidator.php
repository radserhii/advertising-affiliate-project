<?php
declare(strict_types=1);

namespace App\Services;

use DB;
use Illuminate\Mail\Message;
use Illuminate\Support\Collection;
use App\Models\{Account, Advertiser, BalanceTransaction, Lead};

class AdvertiserFinanceSystemValidator
{
    public $errors = [];

    public function validate()
    {
        Advertiser::with('accounts')->chunk(50, function (Collection $advertisers) {
            foreach ($advertisers as $advertiser) {
                $this->compareBalanceWithTransactions($advertiser);
                $this->compareLeadsWithTransactions($advertiser);
            }
        });

        if (\count($this->errors)) {
            $this->sendReport();
        }
    }

    private function compareBalanceWithTransactions(Advertiser $advertiser)
    {
        foreach ($advertiser['accounts'] as $account) {

            $total_transactions = BalanceTransaction::select(
                DB::raw('SUM(balance_sum) AS balance_sum'),
                DB::raw('SUM(system_balance_sum) AS system_balance_sum'),
                DB::raw('SUM(hold_sum) AS hold_sum')
            )
                ->where('user_id', $advertiser['id'])
                ->where('currency_id', $account['currency_id'])
                ->withoutGlobalScopes()
                ->first();

            $balance_sum = (float)$total_transactions['balance_sum'] ?? 0;
            $system_balance_sum = (float)$total_transactions['system_balance_sum'] ?? 0;
            $hold_sum = (float)$total_transactions['hold_sum'] ?? 0;

            if (
                $balance_sum != (float)$account['balance']
                || $system_balance_sum != (float)$account['system_balance']
                || $hold_sum != (float)$account['hold']
            ) {
                $this->generateBalanceWithTransactionComparisonError($advertiser, $account);
            }
        }
    }

    private function compareLeadsWithTransactions(Advertiser $advertiser)
    {
        Lead::where('advertiser_id', $advertiser['id'])
            ->where('is_integrated', 1)
            ->chunk(50, function (Collection $leads) use ($advertiser) {

                foreach ($leads as $lead) {
                    $total = BalanceTransaction::select(
                        DB::raw('SUM(balance_sum) AS balance_sum'),
                        DB::raw('SUM(system_balance_sum) AS system_balance_sum'),
                        DB::raw('SUM(hold_sum) AS hold_sum')
                    )
                        ->where('user_id', $advertiser['id'])
                        ->where('entity_type', BalanceTransaction::LEAD)
                        ->where('entity_id', $lead['id'])
                        ->withoutGlobalScopes()
                        ->first();

                    $balance_sum = (float)$total['balance_sum'] ?? 0;
                    $system_balance_sum = (float)$total['system_balance_sum'] ?? 0;
                    $hold_sum = (float)$total['hold_sum'] ?? 0;

                    $has_error = false;

                    if ($lead['status'] == Lead::NEW) {
                        $has_error = $this->isInvalidTransactionsOfNewLead(
                            $lead,
                            $balance_sum,
                            $system_balance_sum,
                            $hold_sum
                        );

                    } elseif ($lead['status'] == Lead::APPROVED) {
                        $has_error = $this->isInvalidTransactionsOfApprovedLead(
                            $lead,
                            $balance_sum,
                            $system_balance_sum,
                            $hold_sum
                        );

                    } elseif (\in_array($lead['status'], [Lead::CANCELLED, Lead::TRASHED])) {
                        $has_error = $this->isInvalidTransactionsOfCanceledLead(
                            $balance_sum,
                            $system_balance_sum,
                            $hold_sum
                        );
                    }

                    if ($has_error) {
                        $this->generateLeadsWithTransactionComparisonError($advertiser, $lead);
                    }
                }
            });
    }

    private function isInvalidTransactionsOfNewLead(Lead $lead, $balance_sum, $system_balance_sum, $hold_sum)
    {
        return
            $balance_sum != 0
            || $system_balance_sum != 0
            || $hold_sum != (float)$lead['advertiser_payout'];
    }

    private function isInvalidTransactionsOfApprovedLead(Lead $lead, $balance_sum, $system_balance_sum, $hold_sum)
    {
        if (\is_null($lead['advertiser_payout_completed_at'])) {
            return
                $balance_sum + (float)$lead['advertiser_payout'] != 0
                || $system_balance_sum != 0
                || $hold_sum != 0;
        }

        return
            $balance_sum + (float)$lead['advertiser_payout'] != 0
            || $system_balance_sum + (float)$lead['advertiser_payout'] != 0
            || $hold_sum != 0;
    }

    private function isInvalidTransactionsOfCanceledLead($balance_sum, $system_balance_sum, $hold_sum)
    {
        return
            $balance_sum != 0
            || $system_balance_sum != 0
            || $hold_sum != 0;
    }

    private function generateBalanceWithTransactionComparisonError(Advertiser $advertiser, Account $account)
    {
        $error =
            'User ID: ' . $advertiser['id'] .
            ' | Account ID: ' . $account['id'] .
            ' | Currency ID: ' . $account['currency_id'] .
            ' | Error: Advertiser balance is not equal with balance transactions';

        $this->errors[] = $error;
    }

    private function generateLeadsWithTransactionComparisonError(Advertiser $advertiser, Lead $lead)
    {
        $error =
            'User ID: ' . $advertiser['id'] .
            ' | Lead ID: ' . $lead['id'] .
            ' | Error: Advertiser lead has bad balance transactions';

        $this->errors[] = $error;
    }

    private function sendReport()
    {
        \Mail::send([], [], function (Message $message) {
            $message
                ->from(config('env.mail_from'), config('env.mail_sender'))
                ->to('tychko.a@gmail.com')
                ->subject('Finance System Errors')
                ->setBody(\implode("<br>", $this->errors), 'text/html');
        });
    }
}