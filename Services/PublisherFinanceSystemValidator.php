<?php
declare(strict_types=1);

namespace App\Services;

use DB;
use Illuminate\Mail\Message;
use Illuminate\Support\Collection;
use App\Models\{BalanceTransaction, Currency, Lead, Publisher};

class PublisherFinanceSystemValidator
{
    public $errors = [];

    public function validate()
    {
        Publisher::with('profile')->chunk(50, function (Collection $publishers) {
            foreach ($publishers as $publisher) {
                $this->compareBalanceWithTransactions($publisher);
                $this->compareLeadsWithTransactions($publisher);
            }
        });

        if (\count($this->errors)) {
            $this->sendReport();
        }
    }

    private function compareBalanceWithTransactions(Publisher $publisher)
    {
        $total = BalanceTransaction::select(
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::USD_ID . " THEN `balance_sum` ELSE 0 END), 0) as balance_sum_usd"),
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::USD_ID . " THEN `hold_sum` ELSE 0 END), 0) as hold_sum_usd"),
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::RUB_ID . " THEN `balance_sum` ELSE 0 END), 0) as balance_sum_rub"),
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::RUB_ID . " THEN `hold_sum` ELSE 0 END), 0) as hold_sum_rub"),
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::EUR_ID . " THEN `balance_sum` ELSE 0 END), 0) as balance_sum_eur"),
            DB::raw("IFNULL(SUM(CASE WHEN `currency_id` = " . Currency::EUR_ID . " THEN `hold_sum` ELSE 0 END), 0) as hold_sum_eur")
        )
            ->where('user_id', $publisher['id'])
            ->withoutGlobalScopes()
            ->first();

        if (
            $total['balance_sum_usd'] !== $publisher->profile['balance_usd']
            || $total['hold_sum_usd'] !== $publisher->profile['hold_usd']
        ) {
            $this->genInvalidTransactionsOfProfileError($publisher, Currency::USD_ID);
        }

        if (
            $total['balance_sum_rub'] !== $publisher->profile['balance_rub']
            || $total['hold_sum_rub'] !== $publisher->profile['hold_rub']
        ) {
            $this->genInvalidTransactionsOfProfileError($publisher, Currency::RUB_ID);
        }

        if (
            $total['balance_sum_eur'] !== $publisher->profile['balance_eur']
            || $total['hold_sum_eur'] !== $publisher->profile['hold_eur']
        ) {
            $this->genInvalidTransactionsOfProfileError($publisher, Currency::EUR_ID);
        }
    }

    private function genInvalidTransactionsOfProfileError(Publisher $publisher, $currency_id)
    {
        $error =
            'User ID: ' . $publisher['id'] .
            ' | Currency ID: ' . $currency_id .
            ' | Error: Publisher profile balance is not equal with sum balance transactions!';

        $this->errors[] = $error;
    }

    private function compareLeadsWithTransactions(Publisher $publisher)
    {
        Lead::where('publisher_id', $publisher['id'])
            ->where('is_integrated', 1)
            ->chunk(50, function (Collection $leads) use ($publisher) {
                /**
                 * @var Lead $lead
                 */
                foreach ($leads as $lead) {
                    $total = BalanceTransaction::select(
                        DB::raw('IFNULL(SUM(balance_sum), 0) AS balance_sum'),
                        DB::raw('IFNULL(SUM(hold_sum), 0) AS hold_sum')
                    )
                        ->where('user_id', $publisher['id'])
                        ->where('entity_type', BalanceTransaction::LEAD)
                        ->where('entity_id', $lead['id'])
                        ->whereIn('type', [
                            BalanceTransaction::PUBLISHER_HOLD,
                            BalanceTransaction::PUBLISHER_UNHOLD,
                            BalanceTransaction::PUBLISHER_CANCEL
                        ])
                        ->withoutGlobalScopes()
                        ->first();

                    $has_error = false;

                    if (\in_array($lead['status'], [Lead::NEW, Lead::CANCELLED, Lead::TRASHED])) {
                        $has_error = $total['balance_sum'] != 0 || $total['hold_sum'] != 0;
                    }

                    if ($lead['status'] == Lead::APPROVED) {

                        if ($lead->onHold()) {
                            $has_error = $total['balance_sum'] != 0 || $total['hold_sum'] != $lead['payout'];
                        } else {
                            $has_error = $total['balance_sum'] != $lead['payout'] || $total['hold_sum'] != 0;
                        }
                    }

                    if ($has_error) {
                        $this->genInvalidTransactionsOfPublisherLeadError($publisher, $lead);
                    }
                }
            });
    }

    private function genInvalidTransactionsOfPublisherLeadError(Publisher $publisher, Lead $lead)
    {
        $this->errors[] = "UserID:{$publisher['id']} | LeadID: {$lead['id']} | Error: Publisher lead has bad balance transactions";
    }

    private function sendReport()
    {
        \Mail::send([], [], function (Message $message) {
            $message
                ->from(config('env.mail_from'), config('env.mail_sender'))
                ->to('tychko.a@gmail.com')
                ->subject('Finance System Errors')
                ->setBody(\implode("<br>", $this->errors), 'text/html');
        });
    }
}