<?php
declare(strict_types=1);

namespace App\Providers;

use App\Mail as M;
use App\Events as E;
use App\Listeners as L;
use App\Models\{
    Domain, Lead, Target
};
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     */
    protected $listen = [
        E\Flow\FlowCreated::class => [
            L\CreateRouteLimitStat::class,
        ],
    ];

    /**
     * The subscriber classes to register.
     */
    protected $subscribe = [

    ];

    /**
     * Register any other events for your application.
     */
    public function boot()
    {

        parent::boot();
    }
}
