<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteLimitStatsTable extends Migration
{
    public function up()
    {
        Schema::create('route_limit_stats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id');
            $table->string('route', 64);
            $table->unsignedSmallInteger('count');
            $table->timestamp('datetime')->nullable();

            $table->unique(['datetime', 'user_id', 'route']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('route_limit_stats');
    }
}
